import {expect} from 'chai';
import OpenWeatherMapController from '../server/routes/api/controllers/OpenWeatherMapController';


describe('Wipro test tests', () => {


  it('Gathering 5 days forecast test:', function() { // no done

    // note the return
    return OpenWeatherMapController.getFiveDays()
      .then(function(response){
        expect(response.status).to.equal(200);
      });// no catch, it'll figure it out since the promise is rejected
  });

});
