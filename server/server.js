import config from 'config';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
// import expressValidator from 'express-validator';
import flash from 'connect-flash';
import session from 'express-session';
import webRouter from './routes/web/webRouter';
import apiRouter from './routes/api/apiRouter';



const app = express();




/************************* EXPRESS SETTINGS: *************************/

app.set('views', path.join(__dirname, '/../public/views'));

// use twig as the template engine
// Set render engine
app.set('view engine', 'twig');
// This section is optional and used to configure twig.
app.set('twig options', {
  strict_variables: false
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));
app.use(morgan('dev'));
app.use(cookieParser());


// make all assets (.js, .css, images, etc) in public accesible
app.use(express.static(path.join(__dirname, '/../public')));

app.use(session({
  secret: 'ThisIsNotAValidSecretChangeIt',
  saveUninitialized: true,
  resave: true
}));




/**/
// expressValidator is now created straight in the routes

// Connect flash
app.use(flash());

// Global Vars
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});



// To make this server CORS-ENABLE
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


/************************* EXPRESS ROUTERS: *************************/
// API Router
app.use('/api', apiRouter);

// The rest of routes go to react webApp
app.use('/*', webRouter);



/************************* EXPRESS INIT: *************************/
app.listen(config.server.port, () => {
  console.log('Express listening on port ', config.server.port);
});
