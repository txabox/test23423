import express from 'express';

import OpenWeatherMapController from './controllers/OpenWeatherMapController';

const router = express.Router();


router
  .get('/forecast/five/days', (req, res) => {
    OpenWeatherMapController.getFiveDays(res, res)
      .then( (response) => {
        res.status(response.data.cod).json(response.data);
      })
      .catch( (error) => {
        res.status(error.response.data.cod).json(error.response.data);
      });
  });

export default router;
