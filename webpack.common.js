const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'src/App.js'),
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [{
          loader: 'style-loader' // creates style nodes from JS strings
        }, {
          loader: 'css-loader' // translates CSS into CommonJS
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([
      __dirname + '/public'
    ],{
      verbose:  true,
    }),
    new CopyWebpackPlugin([
      {
        from: __dirname + '/src/views',
        to: __dirname + '/public/views',
      },
      {
        from: __dirname + '/src/assets/images',
        to: __dirname + '/public/assets/images'
      },
      {
        from: __dirname + '/src/assets/data',
        to: __dirname + '/public/assets/data'
      }
    ])
  ],
  resolve: {
    alias: {
      Root: path.resolve(__dirname, './'),
      Assets: path.resolve(__dirname, 'src/assets/'),
      Components: path.resolve(__dirname, 'src/components/'),
      Redux: path.resolve(__dirname, 'src/redux/'),
      Actions: path.resolve(__dirname, 'src/redux/actions/'),
    }
  }
};
