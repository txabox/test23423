const defaultValues = {
  zoom: 2000
}

const indexReducer = (state = defaultValues, action) => {
  let newState = {...state}

  if (action.type === 'INDEX_SET_ZOOM') {
    newState.zoom = action.payload.zoom
  }

  return newState
}

export default indexReducer
