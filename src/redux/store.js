import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux'
import thunk from 'redux-thunk'
// Logger with default options
import logger from 'redux-logger'
import indexReducer from './reducers/index/indexReducer'

const reducers = combineReducers({
  index: indexReducer
})

// middlewares:
const mutatingErrorChecker = store => next => action => {
  let prevState = store.getState()
  let result = next(action)
  let curState = store.getState()

  if (process.env.NODE_ENV === 'development' && JSON.stringify(prevState) === JSON.stringify(curState)) {
    console.warn('prevState and curState is exactly the same in action ' + action.type + '. action: ', action, ', state: ', curState)
  }

  return result
}

export default createStore(
  reducers,
  applyMiddleware(thunk, logger, mutatingErrorChecker))
