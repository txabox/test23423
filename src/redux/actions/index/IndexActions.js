class IndexActions {
  setZoom = zoom => {
    return {
      type: 'INDEX_SET_ZOOM',
      payload: {
        zoom
      }
    }
  };
}

export default new IndexActions()
