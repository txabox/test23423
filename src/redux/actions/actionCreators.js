import index from './index/IndexActions'

class ActionCreators {
  constructor () {
    this.index = index
  }
}

export default new ActionCreators()
