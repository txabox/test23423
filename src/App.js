import React from 'react'
import ReactDOM from 'react-dom'
import Router from './Router'

import './app.scss'

if (process.env.NODE_ENV === 'development') {
  console.warn('Looks like we are in development mode!')
}

ReactDOM.render(
  (
    <Router />
  ),
  document.getElementById('testApp')
)
