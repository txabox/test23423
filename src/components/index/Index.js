import React from 'react'
import axios from 'axios'

import store from 'Redux/store'
// import actions from 'Actions/actionCreators';

import LoadingSpinner from 'Components/common/loadingSpinner/LoadingSpinner'
import MapInterface from './components/MapInterface/MapInterface'
import Zoomer from './components/Zoomer/Zoomer'
import FriscoMap from './components/FriscoMap/FriscoMap'
import NextbusMap from './components/NextbusMap/NextbusMap'
import './index.scss'

class Index extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      innerWidth: window.innerWidth,
      innerHeight: window.innerHeight,
      isInitialAjaxDone: false,
      showJsonLoader: false,
      mainMapDivDimensions: {
        width: 800,
        height: 450
      },
      sf: {
        mainLayer: 'neighborhoods',
        layers: [
          'neighborhoods', 'streets', 'freeways', 'arteries'
        ],
        JSONs: {
          neighborhoods: {},
          streets: {},
          freeways: {},
          arteries: {}
        },
        layersVisibility: {
          neighborhoods: true,
          streets: false,
          freeways: false,
          arteries: false
        }
      },
      nextbus: {
        routes: []
      },
      zoom: store.getState().index.zoom,
      viewBoxDimensions: '0 0 0 0'
    }

    this._currentlyUpdating = new Set()
  }

  componentDidMount () {
    // const self = this;
    // create timers, listeners...
    this.unsubscribe = store.subscribe(() => {
      this.setState({zoom: store.getState().index.zoom})
    })

    window.addEventListener('resize', this.updateDimensions)

    this.loadInitialJSONs(() => {
      this.initDrawingRoutes()
    })
  }

  componentWillUnmount () {
    // clean timers, listeners...
    window.removeEventListener('resize', this.updateDimensions)
    this.unsubscribe()
  }

  isEmptyObject = (obj) => Object.keys(obj).length === 0 && obj.constructor === Object;
  isObject = (a) => (!!a) && (a.constructor === Object)

  initDrawingRoutes = () => {
    const updateVehiclePositions = (vehiclesJson, newVehicles) => {
      // if there were no vehicles, but there are new ones...
      if (!vehiclesJson.vehicle) {
        // add the new ones to this vehiclesJson
        vehiclesJson.vehicle = newVehicles
        return false
      }

      for (let i = 0; i < vehiclesJson.vehicle.length; i++) {
        let curVehicle = vehiclesJson.vehicle[i]
        for (let j = 0; j < newVehicles.length; j++) {
          const newVehicle = newVehicles[j]
          if (curVehicle.id === newVehicle.id) {
            vehiclesJson.vehicle[i] = newVehicle
            // exit inner j loop
            break
          }
        }
      }
    }

    const updateVisibleRoutes = () => {
      // to prevent ajax problems while user has toogle route visibility,
      // do not update the routes this cycle.
      if (this._currentlyUpdating.size > 0) {
        return false
      }

      let promisesToUpdate = []
      this.state.nextbus.routes.map((route) => {
        if (route.isVisible) {
          const lastTime = route.vehiclesJson.lastTime
            ? route.vehiclesJson.lastTime.time
            : 0
          promisesToUpdate.push(() => axios.get(route.promise + lastTime))
        }
      })

      Promise.all(promisesToUpdate.map((promise) => promise())).then((response) => {
        // 1. clone the routes to keep it immutable
        const routes = JSON.parse(JSON.stringify(this.state.nextbus.routes))

        // 2. Assign the json to the proper routes
        response.map((routeResponse, i) => {
          // if there is only 1 vehicle the API returns an object instead
          // of an array of 1 element.
          if (this.isObject(routeResponse.data.vehicle)) {
            // so we have to put it inside an array
            routeResponse.data.vehicle = [routeResponse.data.vehicle]
          }

          // if any of the bus have changed their position
          if (routeResponse.data.vehicle && routeResponse.data.vehicle.length > 0) {
            const route = routes[i]

            if (this.isEmptyObject(route.vehiclesJson)) {
              route.vehiclesJson = routeResponse.data
            } else {
              updateVehiclePositions(route.vehiclesJson, routeResponse.data.vehicle)
            }
          }
        })

        this.setState({
          isInitialAjaxDone: true,
          nextbus: {
            ...this.state.nextbus,
            routes
          }
        })
      }).catch((error) => {
        console.error('Error retrieving multiple routes from nextbus API. Error: ', error)
        alert('Error retrieving multiple routes from nextbus API. Look at the console for more info.')
      })
    }

    // draw them the 1st time
    updateVisibleRoutes()
    // redraw them every 15 seconds
    setInterval(updateVisibleRoutes, 15000)
  }

  loadFriscoLayer = (layer) => {
    return axios.get(`assets/data/${layer}.json`)
  }

  loadBusRoutes = () => {
    return axios.get('http://webservices.nextbus.com/service/publicJSONFeed?command=routeList&a=sf-muni').then((response) => {
      //  'http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=sf-muni&r=F'
      let promisesToUpdate = []
      response.data.route.map(route => {
        promisesToUpdate.push(() => axios.get('http://webservices.nextbus.com/service/publicJSONFeed?command=routeConfig&a=sf-muni&r=' + route.tag))
      })

      return Promise.all(promisesToUpdate.map((promise) => promise())).then((response) => {
        let routes = []

        response.map(curResponse => {
          // assign extra route info:
          const route = curResponse.data.route
          route.color = '#' + route.color
          route.oppositeColor = '#' + route.oppositeColor
          route.isVisible = true
          route.promise = `http://webservices.nextbus.com/service/publicJSONFeed?command=vehicleLocations&a=sf-muni&r=${route.tag}&t=`
          route.vehiclesJson = {}
          routes.push(route)
        })

        const _response = {
          data: {
            routes
          }
        }

        return _response
      }).catch((error) => {
        console.error('Error 1 retrieving multiple routes from nextbus API. Error: ', error)
        alert('Error 1 retrieving multiple routes from nextbus API. Look at the console for more info.')
      })
    }).catch((error) => {
      return error
    })
  }

  loadInitialJSONs = (cb) => {
    const self = this

    Promise.all([
      this.loadBusRoutes(),
      this.loadFriscoLayer(this.state.sf.mainLayer)
    ]).then(([busRoutesResponse, mainLayerResponse]) => {
      const routes = busRoutesResponse.data.routes
      const mainLayer = mainLayerResponse.data

      self.setState({
        nextbus: {
          ...self.state.nextbus,
          routes
        },
        sf: {
          ...self.state.sf,
          JSONs: {
            ...self.state.sf.JSONs,
            [this.state.sf.mainLayer]: mainLayer
          }
        }
      }, () => {
        cb()
      })
    }).catch(reason => {
      console.error('Error retrieving JSON: ', reason)
      alert('Error retrieving JSON. Look at the console for more details')
    })
  }

  findRouteByTag = (routes, tag) => routes.find(function (route) {
    return route.tag === tag
  });

  toogleRoute = (tag) => {
    const self = this

    const hideRoute = (tag) => {
      // 1. push tag to the currently loading assets (layers/routes jsons)
      self._currentlyUpdating.add(`hide_${tag}`)

      // 2. show the loader animation
      self.setState({
        showJsonLoader: true
      }, () => {
        // 3. clone the routes to keep them immutable
        const routes = JSON.parse(JSON.stringify(this.state.nextbus.routes))

        // 4. Get the route to modify and modify its visibility
        const route = this.findRouteByTag(routes, tag)
        route.isVisible = !route.isVisible

        // 5. remove the json so that we free memory.
        route.vehiclesJson = {}

        // 6. save the new routes
        self.setState({
          nextbus: {
            ...self.state.nextbus,
            routes
          }
        }, () => {
          // 7. Remove asset from currently loading assets
          self._currentlyUpdating.delete(`hide_${tag}`)
          const showJsonLoader = self._currentlyUpdating.size !== 0

          // 9. And hide the JsonLoader if it was shown and
          // there are no more jsons being loaded
          if (!showJsonLoader) {
            self.setState({showJsonLoader})
          }
        })
      })
    }

    const showRoute = (tag, promise) => {
      // 1. push tag to the currently loading assets (layers/routes jsons)
      self._currentlyUpdating.add(`loading_${tag}`)

      // 2. show the loader animation
      self.setState({
        showJsonLoader: true
      }, () => {
        // 3. Load assets
        axios.get(promise).then((response) => {
          // 4. Check response
          if (response.status !== 200) {
            console.error('Error retrieving the route json. Response: ', response)
            alert('Error retrieving the route json. Look at the console for more info.')
            return false
          }

          // 4. clone the routes to keep them immutable
          const routes = JSON.parse(JSON.stringify(this.state.nextbus.routes))

          // 5. Get the route to modify and modify its visibility
          const route = this.findRouteByTag(routes, tag)
          route.isVisible = !route.isVisible

          // 6. Assign the response json to the route
          route.vehiclesJson = response.data
          // if there is only 1 vehicle the API returns an object instead of an array of 1 element.
          if (self.isObject(response.data.vehicle)) {
            // so we have to put it inside an array
            response.data.vehicle = [response.data.vehicle]
          }

          // 7. Set the new state
          self.setState({
            nextbus: {
              ...self.state.nextbus,
              routes
            }
          }, () => {
            // 8. Remove asset from currently loading assets
            self._currentlyUpdating.delete(`loading_${tag}`)
            const showJsonLoader = self._currentlyUpdating.size !== 0

            // 9. And hide the JsonLoader if it was shown and
            // there are no more jsons being loaded
            if (!showJsonLoader) {
              self.setState({showJsonLoader})
            }
          })
        }).catch((error) => {
          console.error('Error retrieving the route json. Error: ', error)
          alert('Error retrieving the route json. Look at the console for more info.')
        })
      })
    }

    // 2. tempRoute... do not modify this one
    const tempRoute = this.findRouteByTag(this.state.nextbus.routes, tag)
    const isVisible = tempRoute.isVisible

    const promise = tempRoute.promise + '0'

    // 3. Check whether we are hiding or showing the route:
    if (isVisible) {
      hideRoute(tag)
    } else {
      showRoute(tag, promise)
    }
  }

  toogleFriscoLayer = (layer) => {
    const layersVisibility = JSON.parse(JSON.stringify(this.state.sf.layersVisibility))
    layersVisibility[layer] = !layersVisibility[layer]

    // mainLayer is a special layer.
    // it doesn't load external json. Only at init.
    if (layer === this.state.sf.mainLayer) {
      this.setState({
        sf: {
          ...this.state.sf,
          layersVisibility
        }
      })
      return false
    }

    // show friscoLayer
    if (layersVisibility[layer]) {
      this.loadFriscoLayer(layer).then((response) => {
        // Check response
        if (response.status !== 200) {
          console.error('Error retrieving frisco json. Response: ', response)
          alert('Error retrieving frisco json. Look at the console for more info.')
          return false
        }

        this.setState({
          sf: {
            ...this.state.sf,
            layersVisibility,
            JSONs: {
              ...this.state.sf.JSONs,
              [layer]: response.data
            }
          }

        })
      }).catch((error) => {
        console.error('Error retrieving layer. Error: ', error)
      })
    } else {
      // set its visibility to false and empty its json so that it doesn't
      // consume resources.
      this.setState({
        sf: {
          ...this.state.sf,
          layersVisibility,
          JSONs: {
            ...this.state.sf.JSONs,
            [layer]: {}
          }
        }
      })
    }
  }

  stopEventPropagation = (e) => {
    e.stopPropagation()
  }

  updateDimensions = () => {
    this.setState({innerWidth: window.innerWidth, innerHeight: window.innerHeight})
  }

  render () {
    return (<div className="index container">
      <h1>San Francisco Bus Routes</h1>
      {
        this.state.isInitialAjaxDone
          ? <div className="maps-and-interface">

            <div className="maps">
              {
                this.state.sf.layers.map((layer) => {
                  // mainLayer is a sepecial map because we get the
                  // viewBoxDimensions from it. It can't be gone entirely.
                  return (this.state.sf.mainLayer === layer || this.state.sf.layersVisibility[layer])
                    ? <FriscoMap key={`${layer}Map`} getParent={() => {
                      return this
                    }} mainMapDivDimensions={this.state.mainMapDivDimensions} cssClass={layer} json={this.state.sf.JSONs[layer]} viewBoxDimensions={this.state.viewBoxDimensions} isVisible={this.state.sf.layersVisibility[layer]} isMainLayer={layer === this.state.sf.mainLayer}/>
                    : null
                })
              }

              <NextbusMap getParent={() => {
                return this
              }} mainMapDivDimensions={this.state.mainMapDivDimensions} cssClass='nextbus' viewBoxDimensions={this.state.viewBoxDimensions} routes={this.state.nextbus.routes}/>
            </div>

            <MapInterface getParent={() => {
              return this
            }} mainMapDivDimensions={this.state.mainMapDivDimensions} sf={this.state.sf} nextbus={this.state.nextbus}/>

            <Zoomer getParent={() => {
              return this
            }} mainMapDivDimensions={this.state.mainMapDivDimensions}/>

          </div>

          : <div className="loading-spinner" onClick={this.stopEventPropagation} style={{
            height: `${this.state.innerHeight}px`,
            paddingTop: ((this.state.innerHeight / 2) - 30/* half the height of the animation height */) + 'px'
          }}>
            <LoadingSpinner/>
            <div className="text-center margin-top-10px">
                Initializing maps....
            </div>
          </div>
      } {
        this.state.showJsonLoader
          ? <div className="loading-spinner" onClick={this.stopEventPropagation} style={{
            height: `${this.state.innerHeight}px`,
            paddingTop: ((this.state.innerHeight / 2) - 30/* half the height of the animation height */) + 'px'
          }}>
            <LoadingSpinner/>
            <div className="text-center margin-top-10px">
                Updating routes....
            </div>
          </div>
          : null
      }<p style={{
        marginTop: (this.state.mainMapDivDimensions.height + 17) + 'px'
      }}>
        Select the route you want to show on the Bus Layer menu on the left.
      </p>

    </div>)
  }
}

export default Index
