import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ButtonGroup, Glyphicon, Button } from 'react-bootstrap'

import store from 'Redux/store'
import actions from 'Actions/actionCreators'

import './zoomer.scss'

class Zoomer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      zoom: store.getState().index.zoom
    }
    this._ZOOM_INTERVAL = 1000
  }

  componentDidMount () {
    this.unsubscribe = store.subscribe(() => {
      this.setState({
        zoom: store.getState().index.zoom
      })
    })
  }

  componentWillUnmount () {
    // clean timers, listeners...
    this.unsubscribe()
  }

  zoomUp = (e) => {
    e.preventDefault()
    const zoom = this.state.zoom + this._ZOOM_INTERVAL
    store.dispatch(actions.index.setZoom(zoom))
  }

  zoomDown = (e) => {
    e.preventDefault()
    if (this.state.zoom - this._ZOOM_INTERVAL > 0) {
      const zoom = this.state.zoom - this._ZOOM_INTERVAL
      store.dispatch(actions.index.setZoom(zoom))
    } else {
      alert('A zoom lower than 0 is not allowed!')
    }
  }

  render () {
    return <div className="zoomer">

      {
        this.props.mainMapDivDimensions.width > 768
          ? <span className="zoom-span">
            {this.state.zoom}%
          </span>
          : null
      }
      <ButtonGroup bsClass="zoom-btns">
        <Button bsStyle="primary" bsSize="xsmall" onClick={this.zoomDown}>
          <Glyphicon glyph="minus" />
        </Button>
        <Button bsStyle="primary" bsSize="xsmall" onClick={this.zoomUp}>
          <Glyphicon glyph="plus" />
        </Button>
      </ButtonGroup>

    </div>
  }
}

Zoomer.propTypes = {
  getParent: PropTypes.func.isRequired,
  mainMapDivDimensions: PropTypes.object.isRequired
}

export default Zoomer
