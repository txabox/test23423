import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {geoMercator} from 'd3-geo'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'

import store from 'Redux/store'
// import actions from 'Actions/actionCreators';

import './nextbus.scss'

class NextbusMap extends Component {
  constructor (props) {
    super(props)
    this.state = {
      zoom: store.getState().index.zoom
    }
  }

  componentDidMount () {
    this.unsubscribe = store.subscribe(() => {
      this.setState({zoom: store.getState().index.zoom})
    })
  }

  componentWillUnmount () {
    // clean timers, listeners...
    this.unsubscribe()
  }

  projection () {
    return geoMercator().scale(this.state.zoom)
    // .translate([ this.props.mainMapDivDimensions.width / 2, this.props.mainMapDivDimensions.width / 2 ]);
  }

  render () {
    return (<div className="nextbus-map">

      <svg viewBox={this.props.viewBoxDimensions}>

        {
          this.props.routes.map((route) => {
            if (route.isVisible) {
              const getPaths = (route) => {
                const lines = []
                route.path.map((path, i) => {
                  const point = path.point
                  for (let j = 0; j < point.length; j++) {
                    const curPoint = point[j]
                    const nextPoint = point[j + 1]
                    if (!nextPoint) { break }

                    const curPointCoords = [curPoint.lon, curPoint.lat]
                    const nextPointCoords = [nextPoint.lon, nextPoint.lat]
                    const x1 = this.projection()(curPointCoords)[0]
                    const y1 = this.projection()(curPointCoords)[1]
                    const x2 = this.projection()(nextPointCoords)[0]
                    const y2 = this.projection()(nextPointCoords)[1]
                    const lineObj = <line key={`line_${i}_${j}`} x1={x1} y1={y1} x2={x2} y2={y2} stroke={route.color} strokeWidth={0.000006 * this.state.zoom}/>

                    lines.push(lineObj)
                  }
                })
                return lines
              }

              return <g key={`g_paths_${route.tag}`} className={`route ${route.tag}`} style={{
                opacity: 0.5
              }}>
                {getPaths(route)}
              </g>
            }
            return null
          })
        }

        {
          this.props.routes.map((route) => {
            if (route.isVisible) {
              const getVehicles = (route) => {
                if (!route.vehiclesJson.vehicle) {
                  return []
                }

                const vehicles = []
                const tooltip = (route, vehicle) => (<Tooltip id={`tooltip_${vehicle.id}`}>
                  <strong>Vehicle:</strong>&nbsp;#{vehicle.id}<br/>
                  <strong>Route:</strong>&nbsp;{route.title}
                  ({route.tag})<br/>
                  <strong>Speed:</strong>&nbsp;{vehicle.speedKmHr}km/h.
                </Tooltip>)

                route.vehiclesJson.vehicle.map((vehicle) => {
                  const vehicleCoords = [vehicle.lon, vehicle.lat]
                  const cx = this.projection()(vehicleCoords)[0]
                  const cy = this.projection()(vehicleCoords)[1]
                  const r = this.props.mainMapDivDimensions.width > 400
                    ? 0.000015 * this.state.zoom
                    : 0.00005 * this.state.zoom
                  const circle = <OverlayTrigger placement="top" overlay={tooltip(route, vehicle)} key={`${vehicle.id}`}>
                    <circle cx={cx} cy={cy} r={r} fill={route.color} stroke='none' className='vehicle'/>
                  </OverlayTrigger>
                  vehicles.push(circle)
                })
                return vehicles
              }

              return <g key={`g_vehicles_${route.tag}`} className={`route ${route.tag}`}>
                {getVehicles(route)}
              </g>
            }
            return null
          })
        }

      </svg>

    </div>)
  }
}

NextbusMap.propTypes = {
  getParent: PropTypes.func.isRequired,
  mainMapDivDimensions: PropTypes.object.isRequired,
  cssClass: PropTypes.string.isRequired,
  viewBoxDimensions: PropTypes.string.isRequired,
  routes: PropTypes.array.isRequired
}

export default NextbusMap
