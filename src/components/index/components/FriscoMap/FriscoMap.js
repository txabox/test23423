import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { geoMercator } from 'd3-geo'
import Measure from 'react-measure'

import store from 'Redux/store'
// import actions from 'Actions/actionCreators';

import geojson2svg from 'geojson-to-svg'
import renderHTML from 'react-render-html'

import './friscoMap.scss'

class FriscoMap extends Component {
  constructor (props) {
    super(props)
    this.state = {
      svgString: '',
      zoom: store.getState().index.zoom
    }
  }

  componentDidMount () {
    this.unsubscribe = store.subscribe(() => {
      this.setState({
        zoom: store.getState().index.zoom
      }, () => {
        // we have to reset the svg when zoom or viewBoxDimensions change
        this.setSVG(this.props.viewBoxDimensions)
      })
    })

    // set the svg for the 1st time
    this.setSVG(this.props.viewBoxDimensions)
  }

  componentWillUnmount () {
    // clean timers, listeners...
    this.unsubscribe()
  }

  projection () {
    return geoMercator()
      .scale(this.state.zoom)
    // .translate([ this.props.mainMapDivDimensions.width / 2, this.props.mainMapDivDimensions.width / 2 ]);
  }

  setSVG = (viewBoxDimensions) => {
    const self = this

    // 1. Create the svg string object from geojson2svg
    let svgString = geojson2svg()
      .projection((coord) => {
        return self.projection()(coord)
      })
      // .extent( [-1837.9542507835465, -567.7319837719583, 2.8373886946776565, 2.376727602562255] )
      // .styles({ 'FeatureCollection' : { fill: 'blue', stroke: 'red', strokeWidth:'0.001' } })
      .render(this.props.json)

    // 2. Create the html svg node so that we can retrieve the viewbox from there
    const svgObj = renderHTML(svgString)

    // 3. Get the cur viewbox dimensions
    const curViewBox = svgObj.props.viewBox

    // 4. Set all the svg viewbox
    // we set the viewbox of all the svgs from the street svg
    if (this.props.isMainLayer &&
      this.props.getParent().state.viewBoxDimensions !== curViewBox) {
      // store.dispatch(actions.index.setViewBoxDimensions(curViewBox));
      this.props.getParent().setState({
        viewBoxDimensions: curViewBox
      })
    } else {
      svgString = svgString.replace(curViewBox, viewBoxDimensions)
    }

    // 5. Finally, store the new svgString with the proper viewbox
    this.setState({
      svgString
    })
  }

  componentWillReceiveProps= (nextProps) => {
    if (!this.props.isMainLayer && this.props.viewBoxDimensions !== nextProps.viewBoxDimensions) {
      this.setSVG(nextProps.viewBoxDimensions)
    }
  }

  updateMapsDivDimensions = (mainMapDivDimensions) => {
    if (this.props.isMainLayer) {
      this.props.getParent().setState({
        mainMapDivDimensions
      })
    }
  }

  render () {
    return <Measure
      bounds
      onResize={(contentRect) => {
        this.updateMapsDivDimensions(contentRect.bounds)
      }}
    >
      {({ measureRef }) =>
        <div ref={measureRef} className={`frisco-map ${this.props.cssClass}` + (this.props.isVisible ? '' : ' invisible')}>
          {renderHTML(this.state.svgString)}
        </div>
      }
    </Measure>
  }
}

FriscoMap.propTypes = {
  getParent: PropTypes.func.isRequired,
  mainMapDivDimensions: PropTypes.object.isRequired,
  cssClass: PropTypes.string.isRequired,
  json: PropTypes.object.isRequired,
  viewBoxDimensions: PropTypes.string.isRequired,
  isVisible: PropTypes.bool.isRequired,
  isMainLayer: PropTypes.bool.isRequired
}

export default FriscoMap
