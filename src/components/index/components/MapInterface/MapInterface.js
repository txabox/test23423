import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Glyphicon, Button, Table } from 'react-bootstrap'

import './mapInterface.scss'

class MapInterface extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: true,
      friscoLayersVisibility: true,
      busLayersVisibility: true
    }
  }

  toogleIsOpen = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  toogleFriscoLayersVisibility = () => {
    this.setState({
      friscoLayersVisibility: !this.state.friscoLayersVisibility
    })
  }

  toogleBusLayersVisibility = () => {
    this.setState({
      busLayersVisibility: !this.state.busLayersVisibility
    })
  }

  render () {
    return <div className="map-interface">
      {
        this.state.isOpen
          ? <div className="is-open">
            <div className="scrollable-content" style={{
              maxHeight: (this.props.mainMapDivDimensions.height - 24 /* container padding + margin-bottom */) + 'px'
            }}>

              <div className="friscoLayers">
                <h5 onClick={ this.toogleFriscoLayersVisibility}>
                  San Francisco Layers:
                  <Button bsStyle="link" bsSize="xsmall">
                    <Glyphicon glyph={this.state.friscoLayersVisibility ? 'chevron-up' : 'chevron-down'} />
                  </Button>
                </h5>
                {
                  this.state.friscoLayersVisibility
                    ? <Table striped bordered condensed responsive>
                      <tbody>

                        {
                          this.props.sf.layers.map(
                            (layer) => {
                              return <tr key={`${layer}Tr`}>
                                <th>
                                  <Button bsStyle={this.props.sf.layersVisibility[layer] ? 'success' : 'default'} bsSize="xsmall" onClick={() => this.props.getParent().toogleFriscoLayer(layer)}>
                                    <Glyphicon glyph={this.props.sf.layersVisibility[layer] ? 'eye-open' : 'eye-close'} />
                                  </Button>
                                </th>
                                <td onClick={() => this.props.getParent().toogleFriscoLayer(layer)}>{layer}</td>
                              </tr>
                            })
                        }

                      </tbody>
                    </Table>
                    : null
                }

              </div>

              <div className="busLayers">
                <h5 onClick={ this.toogleBusLayersVisibility}>
                  Bus Layers:
                  <Button bsStyle="link" bsSize="xsmall">
                    <Glyphicon glyph={this.state.busLayersVisibility ? 'chevron-up' : 'chevron-down'} />
                  </Button>
                </h5>
                {
                  this.state.busLayersVisibility
                    ? <Table striped bordered condensed responsive>
                      <tbody>

                        {
                          this.props.nextbus.routes.map(
                            (route) => {
                              return <tr key={`${route.tag}Tr`}>
                                <th>
                                  <Button bsStyle={route.isVisible ? 'success' : 'default'} bsSize="xsmall" onClick={() => this.props.getParent().toogleRoute(route.tag)}>
                                    <Glyphicon glyph={route.isVisible ? 'eye-open' : 'eye-close'} />
                                  </Button>
                                </th>
                                <td style={{
                                  backgroundColor: `${route.color}`,
                                  color: `${route.oppositeColor}`
                                }} onClick={() => this.props.getParent().toogleRoute(route.tag)}>
                                  {
                                    this.props.mainMapDivDimensions.width > 768
                                      ? <div>
                                        {route.title} ({route.tag})
                                      </div>
                                      : <div>
                                        {route.tag}
                                      </div>
                                  }

                                </td>
                              </tr>
                            })
                        }

                      </tbody>
                    </Table>
                    : null
                }

              </div>

            </div>

            <div className="close-interface text-center">
              <Button bsSize="xsmall" bsStyle="link" onClick={this.toogleIsOpen}>
                <Glyphicon glyph='chevron-left' />
              </Button>
            </div>

          </div>

          : <div className="isNotOpen">
            <Button bsSize="xsmall" bsStyle="link" onClick={this.toogleIsOpen}>
              <Glyphicon glyph='chevron-right' /> Open
            </Button>
          </div>

      }

    </div>
  }
}

MapInterface.propTypes = {
  getParent: PropTypes.func.isRequired,
  sf: PropTypes.object.isRequired,
  nextbus: PropTypes.object.isRequired,
  mainMapDivDimensions: PropTypes.object.isRequired
}

export default MapInterface
