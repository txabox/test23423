import React, { Component } from 'react';
import { ScaleLoader } from 'halogenium';

import './loadingSpinner.scss';

class LoadingSpinner extends Component {

  render() {

    return (
      <div className="LoadingSpinner">
        <div className="loading">
          <div className="loading-internal">
            <ScaleLoader color='#666'/>
          </div>
        </div>
      </div>
    );
  }

}

export default LoadingSpinner;
